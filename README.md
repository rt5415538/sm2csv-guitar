# SM2CSV GUITAR

SM2CSV para el modo guitarra de Audition Online

Este conversor soporta muchas cosas (gracias Ais Hikki por recordarme sobre el estilo swing x3 TFC :D )

- Notas tipo 192
- 4k/8k notas
- Notas tipo escalera
- Bomb Swing
- Multibpm
- Metadatos: Total MADI, Beatline y Forzar BPM, Creditos del autor
- Estilo de swing de TFC (usando bomb swing)
- Soporte para arhivos ".ssc"

# ANTES DE CONVERTIR

Debes de colocar estos datos en el sm o ssc antes de la conversión:
- Total MADI: según cuantos MADI ha durado la canción
- BEATLINE: normalmente usan solo el "4"
- CREDITS: tu nombre o tu apodo xd

Si tu sm tiene multibpm, debes usar FORCE BPM para colocar el bpm resultante que desees, ya que el conversor no sabe a que bpm convertir porque existen muchos bpm en tu archivo.

![Alt text](datos_sm_scc.jpg)
![Alt text](ejemplo.jpg)

# FORZAR BPM

Jugando con el BEATLINE puedes hacer cosas extrañas en modo guitarra.

Casi todas los charts de guitarra usan BEATLINE 4. Tambien puedes usar otro tipo de beatline, pero si usas otro BEATLINE lo que haras es solamente modificar la distancia entre las líneas del traste de guitarra pero no la velocidad de las notas.

Para ajustar las notas a la nueva BEATLINE lo que debes hacer es usar FORCE BPM y establecer la velocidad que las notas tendrán en ese nuevo BEATLINE usando la fórmula:

FORCE BPM = (ORIGINAL BPM*4)/BEATLINE

ORIGINAL BPM = 134,090

![Alt text](force_bpm_ejemplo.jpg)

# MODIFICAR EL ESTILO DE SWING (BOMB SWING)

Por defecto, la configuración de bomb swing es seteada para toda el chart es:

- Inicio de swing: -3
- Final swing (nota): +14
- Final swing (cola): +2

Esta configuración se puede modificar en el apartado "Artist" de Arrow Vortex, colocando la configuración de este modo: X-X-X, donde los valores "X" son reemplazados a la configuración que quieras

# COMO USAR EL CONVERSOR

Clic en CiciGOD

[![](Cici_Commend.webp)](https://web.facebook.com/100008191806927/videos/811507247599143/)


