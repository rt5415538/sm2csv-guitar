import random
import math
import re
import os
import time
from fractions import Fraction

print("*************************************")
print("**        SM2CSV VERSION RT        **")
print("*************************************\n")

nombre_archivo = ""

nombre_creador = ""

tipo_csv = ""

def abrir_sm():
    
    global nombre_archivo
    lista = os.listdir() #capturamos todos los archivos de la ubicación de la carpeta
    lista_de_nombres = [] #se guardarán solo los archivos ".sm"
    
    for x in lista:
        if x.endswith(".sm") or x.endswith(".ssc"):
            #nombre, extension = os.path.splitext(x)
            lista_de_nombres.append(x)
    
    if lista_de_nombres:
        for x in range(len(lista_de_nombres)):
            print("["+str(x+1)+"] "+lista_de_nombres[x])
    
    else:
        
        print("No hay ningún archivo sm o scc :v\n")
        exit()
    
    orden_archivo = int(input("\nEscoja el archivo a convertir [número]: "))
    nombre_archivo = lista_de_nombres[orden_archivo-1]
    
    with open(nombre_archivo,"r") as sm:
        
        lineas = sm.readlines()
        
    nombre, extension = os.path.splitext(nombre_archivo)
    nombre_archivo = nombre
        
    return lineas

def extraer_notas(datos_sm):
    
    global nombre_creador, tipo_csv
    
    for busqueda_datos in datos_sm:
        
        if "#CREDIT:" in busqueda_datos:
            
            nombre_creador = re.findall("#CREDIT:(.+);", busqueda_datos)[0]
            
        elif "#TITLE:" in busqueda_datos:
            
            cantidad_madi = re.findall("#TITLE:(.+);", busqueda_datos)[0]
            
            try:
            
                cantidad_madi = re.findall("(.+)[+-](.+)[+-](.+)", cantidad_madi)[0][1]
            
            except Exception:
                
                pass
            
        elif "#SUBTITLE:" in busqueda_datos:
            
            if "-" in re.findall("#SUBTITLE:(.+);", busqueda_datos)[0]:
            
                tipo_csv_and_bpm = re.findall("#SUBTITLE:(.+);", busqueda_datos)[0]
                
                tipo_csv = int(tipo_csv_and_bpm.split("-")[0])
                
                forzar_bpm = float(tipo_csv_and_bpm.split("-")[1])
            
            else:
                
                tipo_csv = int(re.findall("#SUBTITLE:(.+);", busqueda_datos)[0])
                
                forzar_bpm = ""
        
        elif "#ARTIST:" in busqueda_datos:
            
            try:
               
                estilo_swingx3 = re.findall("#ARTIST:(.+);", busqueda_datos)[0]
                
                estilo_swingx3 = re.findall("(.+)[+-](.+)[+-](.+)", estilo_swingx3)[0]
                
                swingx3_inicio = int(estilo_swingx3[0])
                
                swingx3_final = int(estilo_swingx3[1])
                
                swingx3_final_cola = int(estilo_swingx3[2])
            
            except Exception:
               
                swingx3_inicio = 3
                
                swingx3_final = 14
                
                swingx3_final_cola = 2
                 
            break
        
    try:
        dato_sm_ssc = "     0,0,0,0,0:\n"
        dato_1_sm = datos_sm.index(dato_sm_ssc) + 1 #se busca la ubicacion de este valor "0,0,0,0,0:" +1
    
    except:
        dato_sm_ssc = "#NOTES:\n"
        dato_1_sm = datos_sm.index(dato_sm_ssc) + 1 #se busca la ubicacion de este valor "0,0,0,0,0:" +1
        
    dato_2_sm = datos_sm.index(";\n",dato_1_sm) + 1 #se busca la ubicación de este valor ";" +1
    
    
    if datos_sm[dato_2_sm:]:
        
        index_chart1_a = dato_1_sm
        
        index_chart1_b = dato_2_sm
        
        notas_1 = [notas_1.replace("\n", "") for notas_1 in datos_sm[index_chart1_a:index_chart1_b]]
        
        seccion_madi_1 = []
        
        madis_1 = []
        
        for nota in notas_1:
            
            if nota == "," or nota == ";":
                
                madis_1.append(seccion_madi_1)
                
                seccion_madi_1 = []
                
            else:
                
                seccion_madi_1.append(nota)
        
        index_chart2_a = datos_sm.index(dato_sm_ssc, index_chart1_b) + 1
        
        index_chart2_b = datos_sm.index(";\n", index_chart2_a) + 1
        
        notas_2 = [notas_2.replace("\n", "") for notas_2 in datos_sm[index_chart2_a:index_chart2_b]]
        
        seccion_madi_2 = []
        
        madis_2 = []
        
        for nota in notas_2:
            
            if nota == "," or nota == ";":
                
                madis_2.append(seccion_madi_2)
                
                seccion_madi_2 = []
                
            else:
                
                seccion_madi_2.append(nota)
                
        #aqui se ordena las dos secciones de chart en uno solo de 16 columnas
        
        madis = []
        
        for x in range((len(madis_2))):
            
            largo_1 = len(madis_1[x]) #cuando termine el algoritmo de eulices, aqui se va a guardar el MCD "maximo comun divisor"
            
            largo_2 = len(madis_2[x])
            
            #algoritmo de euclides para buscar "maximo comun divisor"
            
            while largo_2 != 0:
                
                c = [largo_1//largo_2,largo_1%largo_2]
                
                largo_1 = largo_2
                
                largo_2 = c[1]
                
            mcm = (len(madis_1[x])*len(madis_2[x]))//largo_1 #calculo para "minimo comun multiplo"
            
            calculo_posicion_nota_1 = 0
            
            calculo_posicion_nota_2 = 0
            
            madi_resultante = []
            
            for y in range(mcm):
                
                if y == int(mcm/len(madis_1[x]))*calculo_posicion_nota_1:
                    
                    madi_resultante.insert(y, madis_1[x][calculo_posicion_nota_1])
                    
                    calculo_posicion_nota_1 += 1
                    
                else:
                    
                    madi_resultante.insert(y, "00000000")
                    
                if y == int(mcm/len(madis_2[x]))*calculo_posicion_nota_2:
                    
                    madi_resultante[y] += madis_2[x][calculo_posicion_nota_2]
                    
                    calculo_posicion_nota_2 += 1
                    
                else:
                    
                    madi_resultante[y] += "00000000"
                    
            madis.append(madi_resultante)
        
        #a todas las seccion_madi se les coloca 16 notas para evitar bugs a futuros
        
        for temp_y in madis_1[len(madis_2):]:
            
            for temp_num_x in range(len(temp_y)):
                
                temp_y[temp_num_x] += "00000000"
        
        madis.extend(madis_1[len(madis_2):])
        
    else:
    
        notas = [notas.replace("\n", "") for notas in datos_sm[dato_1_sm:]]
        
        seccion_madi = []
        
        madis = []
        
        for nota in notas:
            
            if nota == "," or nota == ";":
                
                madis.append(seccion_madi)
                
                seccion_madi = []
            
            else:
                
                seccion_madi.append(nota)
        
    #print(madis)
    
    #cantidad_madi = len(madis)
    
    #CALCULOS DEL MULTIBPM
    
    for busqueda_bpms in datos_sm:
        
        if re.findall("(#BPMS:.+)", busqueda_bpms):
            
            break
    
    if forzar_bpm:
        
        bpm_resultante = (Fraction(str(tipo_csv))/Fraction("4"))*Fraction(str(forzar_bpm))
        
        bpm_resultante = round(float(bpm_resultante), 3)
        
    else:
        
        bpm = re.findall("#BPMS:0.000=([0-9.]+);", busqueda_bpms)[0]
        
        bpm_resultante = (Fraction(str(tipo_csv))/Fraction("4"))*Fraction(str(bpm))
        
        bpm_resultante = round(float(bpm_resultante), 3)
        
    if (re.findall("([;])", busqueda_bpms) and
        float(re.findall("#BPMS:0.000=([0-9.]+);", busqueda_bpms)[0]) == bpm_resultante):
        
        bpm = bpm_resultante
    
    #CALCULO MULTBPM    
    
    else:
        
        if re.findall("#BPMS:0.000=([0-9.]+);", busqueda_bpms):
            
            bpms = datos_sm[datos_sm.index(busqueda_bpms):datos_sm.index(busqueda_bpms)+1]
            
        else:
            
            bpms = datos_sm[datos_sm.index(busqueda_bpms):datos_sm.index(";\n")]
        
        bpm = bpm_resultante
        
        keys_bpm = {}
        
        for area_bpm in bpms:
            
            keys_bpm.setdefault(float(re.findall("(?![#BPMS:,])([0-9.]+)=", area_bpm)[0]),float(re.findall("=([0-9.]+)", area_bpm)[0]))
        
        multibpm = 0
        
        aumento_tiempo = 0
        
        tiempo = 0
        
        tiempo_nota = []
        
        #VARIABLES PARA LA BUSQUEDA DEL KEY DEL MULTIBPM
        #aumento_valor_multibpm_key y valor_key_multibpm
        
        aumento_valor_multibpm_key = Fraction("1")/Fraction("48")
        
        valor_key_multibpm = -aumento_valor_multibpm_key
        
        #PRIMERA PARTE CONVERSOR MULTIBPM
        z = time.time()
        for seccion_madi in madis:
            
            cantidad_filas_madi = len(seccion_madi)
            
            #aumento_numero_cambio_multibpm = int(cantidad_filas_madi/4)
            
            busqueda_notas_multibpm = int(192/cantidad_filas_madi)
            
            #print("\n",aumento_numero_cambio_multibpm,"\n")
            
            for x in range(192):
                
                valor_key_multibpm += aumento_valor_multibpm_key
                
                resultado_valor_key_multibpm_decimal = float(valor_key_multibpm)+0.0001
                
                resultado_valor_key_multibpm_decimal = round(resultado_valor_key_multibpm_decimal,3)
                
                multibpm = keys_bpm.get(resultado_valor_key_multibpm_decimal,0)
                
                if multibpm != 0:
                
                    aumento_tiempo = (Fraction("60")/Fraction(str(multibpm)))/Fraction("48")
                
                tiempo += aumento_tiempo
                
                #print(multibpm ,float(tiempo - aumento_tiempo))
                
                if x%busqueda_notas_multibpm == 0:
                    
                    obtener_posicion_nota_en_lista = int(x/busqueda_notas_multibpm)
                    
                    if seccion_madi[obtener_posicion_nota_en_lista] != "00000000" and seccion_madi[obtener_posicion_nota_en_lista] != "0000000000000000":
                        
                        tiempo_nota.append([float(tiempo - aumento_tiempo),seccion_madi[obtener_posicion_nota_en_lista]])
                    
                        #print(float(tiempo - aumento_tiempo) , seccion_madi[obtener_posicion_nota_en_lista])
        print(time.time()-z)
        #SEGUNDA PARTE CONVERSOR MULTIBPM
        
        tiempos_bpm_resultante = [0]
        
        calculo_tiempo_aumento_bpm_resultante = (Fraction("60")/Fraction(str(bpm)))/Fraction("48")
        
        aumento_tiempo_bpm_resultante = 0
        
        #calcular los tiempo del bpm resultante
        
        while tiempos_bpm_resultante[-1] <= tiempo_nota[-1][0]:
            
            aumento_tiempo_bpm_resultante += calculo_tiempo_aumento_bpm_resultante
            
            tiempos_bpm_resultante.append(float(aumento_tiempo_bpm_resultante))
            
        aumento_tiempo_bpm_resultante += calculo_tiempo_aumento_bpm_resultante
        
        tiempos_bpm_resultante.append(float(aumento_tiempo_bpm_resultante))
        
        #TERCERA PARTE CONVERSOR MULTIBPM
        
        #buscar el número mas cercano a los tiempo de las notas en el bpm resultante
        a = time.time()
        numeros_cercanos_temporales = []
        
        guardar_variables = []
        
        posicion_tiempo_bpm_resultante = 0
        
        set_posicion_tiempo_bpm_resultante = 0
        
        for operacion in tiempo_nota:
            
            valor = operacion.pop(0)
            
            for tiempos_bpm in tiempos_bpm_resultante[set_posicion_tiempo_bpm_resultante:]:
                
                posicion_tiempo_bpm_resultante += 1
                numeros_cercanos_temporales.append(abs(tiempos_bpm-valor))
                guardar_variables.append(tiempos_bpm)
                
                if len(numeros_cercanos_temporales) > 2:
                
                    if numeros_cercanos_temporales[-2] < numeros_cercanos_temporales[-1]:
                        
                        operacion.insert(0, float(guardar_variables[-2]))
                        
                        set_posicion_tiempo_bpm_resultante = posicion_tiempo_bpm_resultante
                        
                        numeros_cercanos_temporales.clear()
                        guardar_variables.clear()
                        
                        break
                    
            #operacion.insert(0, float(min(tiempos_bpm_resultante, key=lambda x:abs(x-valor))))
            
            #print(operacion[0])
        print(time.time()-a)
        #CUARTA PARTE DEL MULTIBPM
        
        madis = []
        
        seccion_madi = []
        
        tiempos_bpm_resultante.reverse()
        
        tiempo_nota.reverse()
        
        while tiempo_nota:
            
            if len(seccion_madi) != 192:
            
                if tiempos_bpm_resultante.pop() != tiempo_nota[-1][0]:
                    
                    seccion_madi.append("00000000")
                    
                else:
                    
                    seccion_madi.append(tiempo_nota.pop()[1])
            
            else:
                
                madis.append(seccion_madi)
                
                seccion_madi = []
                
        if len(seccion_madi) <= 192 and len(seccion_madi) > 0:
            
            for añadir_valores in range(192-len(seccion_madi)):
                
                seccion_madi.append("00000000")
                
            madis.append(seccion_madi)
            
            seccion_madi = []
    
    return madis,bpm,cantidad_madi, swingx3_inicio, swingx3_final, swingx3_final_cola

def convertir_notas(all_notas, swingx3_inicio = 3 , swingx3_final = 14, swingx3_final_cola = 2):
    
    #swingx3 inicio, final, final_cola son variables para estilo de swingx3
    
    global tipo_csv
    
    resolucion = 48*1    #resolución de las notas
    
    ubicacion_nota = [0,0,0]    #ubicación de la nota de sm en csv
    
    valor_raro = -7000  #valor random para colocar en csv
    
    temp_swingx3_notas = [] #cuando un chart tiene swingx3, las notas de los demas charts se van a colocar aqui, hasta que
                            #chart con el swing iniciante finalice.
    
    valores_random = [192,142,92,42]    #valor segun la posición de la nota para csv
    
    notas_cola = [[],[],[],[]]
    
    nota = []
    
    notas = []  #las notas que se van a ir colocando para el csv
    
    nota_solo = [0,0]
    
    temporal_final_bomb_swing = []
    
    bomb_swing = []
    
    swing_x3 = []
    
    swing_x2 = [0,0]
    
    nota_rojo = [0,0]
    
    cantidad_notas = 0
    
    total_swingx3 = 0
    
    swingx3_activador_conteo = 0
    
    swingx3_activador_2_conteo = 0
    
    for seccion_madi in all_notas:
        
        cantidad_filas_madi = len(seccion_madi)  #cuantas filas hay en un MADI
        
        #print(cantidad_filas_madi)
        
        aumento_nota = int(resolucion/(cantidad_filas_madi/4))   #calcular el aumento segundo la cantidad de filas y la resolución
        
        #print(aumento_nota)
        
        for fila_nota in seccion_madi:    #haremos algo para cada fila
            
            #codigo para ordenar las notas si hay otros charts en el sm
            #12345678 -> 57812346
            #a2345678b2345678 -> 578b2346578a2346
            
            fila_nota = list(fila_nota)
            
            lista_1_fila_nota = []
            
            lista_2_fila_nota = []
            
            lista_3_fila_nota = []
            
            for x in range(len(fila_nota)):
                
                lista_1_fila_nota.append(fila_nota[x])
                
                if (x+1) % 4 == 0:
                    lista_1_fila_nota.reverse()
                    
                    for y in lista_1_fila_nota:
                        lista_2_fila_nota.insert(0, y)
                        
                    lista_1_fila_nota.clear()
                    
                if (x+1) % 8 == 0:
                    lista_2_fila_nota.append(lista_2_fila_nota.pop(1))
                    lista_3_fila_nota.append(lista_2_fila_nota.copy())
                    lista_2_fila_nota.clear()
                    
            lista_3_fila_nota.reverse()
            
            #fila_nota = [nota_y for notas_x in lista_3_fila_nota for nota_y in notas_x]
            
            fila_nota = lista_3_fila_nota.copy()
            
            cantidad_charts_fila = len(fila_nota) #cantidad de charts en una fila
            
            chart_notas = [nota for temporal_chart in fila_nota for nota in temporal_chart[3:7]]
            
            chart_swingx3 = [nota for temporal_chart in fila_nota for nota in temporal_chart[7]]
            
            chart_notas_swingx3 = chart_notas+chart_swingx3
            
            #setear la cantidad de modificadores segun la cantidad de charts (solo una vez,
            #para esto se verifica que "ubicacion_nota" sea el iniciante, cuando no sea 0,0,0,
            #ya no se volverá a setear)
            
            if ubicacion_nota == [0,0,0]:
                
                nota_solo = [[0,0] for repetir in range(cantidad_charts_fila)]
                
                swing_x2 = [[0,0] for repetir in range(cantidad_charts_fila)]
                
                nota_rojo = [[0,0] for repetir in range(cantidad_charts_fila)]
                
                notas_cola = [[[],[],[],[]] for repetir in range(cantidad_charts_fila)]
                
                bomb_swing = [[] for repetir in range(cantidad_charts_fila)]
                
                temporal_final_bomb_swing = [[] for repetir in range(cantidad_charts_fila)]
                
                temp_swingx3_notas =[[0,[],[]] for repetir in range(cantidad_charts_fila)]
            
            for index_chart_swingx3 in range(len(chart_swingx3)):
                
                if chart_swingx3[index_chart_swingx3] == "1" or chart_swingx3[index_chart_swingx3] == "M" or chart_swingx3[index_chart_swingx3] == "2":
                    
                    temp_swingx3_notas[index_chart_swingx3][0] = 1
            
            for columna_chart in range(cantidad_charts_fila):
                
                chart = fila_nota[columna_chart]
                
                #print(chart)
            
                for columna_nota in range(len(chart)):   #haremos algo para los 8 primeros elementos de la fila(notas)
                    
                    match columna_nota:
                        
                        case 0:
                        
                        #codigo nota_solo
                            
                            if chart[columna_nota] in ["1","2","3"]:
                                
                                #nota_solo.insert([1,fila_nota[columna_nota]])
                                
                                nota_solo[columna_chart] = [1, chart[columna_nota]]
                                
                            elif chart[columna_nota] == "0" and (nota_solo[columna_chart][1] in ["1","3"]):
                                
                                nota_solo[columna_chart] = [0,0]
                                
                        #codigo swing_x3
                        

                        case 7:
                            
                            aumento_disminucion_nota = int(resolucion/48)
                            
                            #parte del código de cola bomb_swing x3 (si nota es "2" o "4")
                            
                            #if len(temporal_final_bomb_swing[columna_chart]) == 1 and ("3" in chart[3:7]):
                            if len(temporal_final_bomb_swing[columna_chart]) == 1 and ("3" in chart_notas):
                                
                                temporal_final_bomb_swing[columna_chart].clear()
                                
                                cantidad_notas += 1
                                
                                bomb_swing[columna_chart].insert(0, 1)
                                
                                #aleatorio = random.choice(valores_random)
                                
                                bomb_swing[columna_chart].insert(0, "12,"+str(42)+","+str(valor_raro)+","+str(242)+","+str(valor_raro-10))
                                
                                valor_raro -= 25
                                
                                ubicacion_swing = ubicacion_nota.copy()
                                
                                for x in range(math.ceil((swingx3_final_cola/4)*tipo_csv)):
                                    
                                    ubicacion_swing[2] += aumento_disminucion_nota
                                    
                                    if ubicacion_swing[2] == resolucion:
                                        
                                        ubicacion_swing[1] += 1
                                        
                                        ubicacion_swing[2] = 0
                                        
                                    if ubicacion_swing[1] == tipo_csv:
                                        
                                        ubicacion_swing[0] += 1
                                        
                                        ubicacion_swing[1] = 0
                                        
                                bomb_swing[columna_chart].append(ubicacion_swing[0])
                                
                                bomb_swing[columna_chart].append(ubicacion_swing[1])
                                
                                bomb_swing[columna_chart].append(ubicacion_swing[2])
                                
                                for x in range(len(bomb_swing[columna_chart])):
                                    
                                    swing_x3.insert(0, bomb_swing[columna_chart].pop())
                                    
                                temp_swingx3_notas[columna_chart][0] = 0
                                
                                    
                            #parte del código de cola bomb_swing x3 (si nota es 1,M,2,4 o swing es 1,2,M)
                            
                            if len(temporal_final_bomb_swing[columna_chart]) > 1 and ("1" in chart_notas_swingx3 or "2" in chart_notas_swingx3 or "M" in chart_notas_swingx3 or "4" in chart_notas_swingx3):
                                
                                var1 = ubicacion_nota[0]*tipo_csv*resolucion + ubicacion_nota[1]*resolucion + ubicacion_nota[2]
                                
                                var2 = temporal_final_bomb_swing[columna_chart][0]*tipo_csv*resolucion + temporal_final_bomb_swing[columna_chart][1]*resolucion + temporal_final_bomb_swing[columna_chart][2]
                                
                                diferencia = var1 - var2
                                
                                if diferencia >= (tipo_csv*aumento_disminucion_nota):
                                    
                                    bomb_swing[columna_chart].append(temporal_final_bomb_swing[columna_chart][0])
                                    
                                    bomb_swing[columna_chart].append(temporal_final_bomb_swing[columna_chart][1])
                                    
                                    bomb_swing[columna_chart].append(temporal_final_bomb_swing[columna_chart][2])
                                    
                                    temporal_final_bomb_swing[columna_chart].clear()
                                    
                                else:
                                    
                                    temporal_final_bomb_swing[columna_chart].clear()
                                    
                                    ubicacion_swing = ubicacion_nota.copy()
                                    
                                    for x in range(math.ceil((swingx3_inicio/4)*tipo_csv)):
                                        
                                        ubicacion_swing[2] -= aumento_disminucion_nota
                                        
                                        if ubicacion_swing[2] == -aumento_disminucion_nota:
                                            
                                            ubicacion_swing[1] -= 1
                                            
                                            ubicacion_swing[2] = resolucion - aumento_disminucion_nota
                                            
                                        if ubicacion_swing[1] == -1:
                                            
                                            ubicacion_swing[0] -= 1
                                            
                                            ubicacion_swing[1] = tipo_csv - 1
                                            
                                    bomb_swing[columna_chart].append(ubicacion_swing[0])
                                    
                                    bomb_swing[columna_chart].append(ubicacion_swing[1])
                                    
                                    bomb_swing[columna_chart].append(ubicacion_swing[2])
                                
                                for x in range(len(bomb_swing[columna_chart])):
                                    
                                    swing_x3.insert(0, bomb_swing[columna_chart].pop())
                                    
                                
                                temp_swingx3_notas[columna_chart][0] = 0
                                
                            #cabeza bomb_swing x3
                            
                            if chart[columna_nota] == "M":
                                
                                if bomb_swing[columna_chart]:
                                    
                                    bomb_swing[columna_chart].clear()
                                    
                                if ("1" in chart_notas or "M" in chart_notas or "2" in chart_notas or "4" in chart_notas):
                                    
                                    ubicacion_swing = ubicacion_nota.copy()
                                    
                                    
                                    
                                    for x in range(math.ceil((swingx3_inicio/4)*tipo_csv)):
                                        
                                        ubicacion_swing[2] -= aumento_disminucion_nota
                                        
                                        if ubicacion_swing[2] == -aumento_disminucion_nota:
                                            
                                            ubicacion_swing[1] -= 1
                                            
                                            ubicacion_swing[2] = resolucion - aumento_disminucion_nota
                                            
                                        if ubicacion_swing[1] == -1:
                                            
                                            ubicacion_swing[0] -= 1
                                            
                                            ubicacion_swing[1] = tipo_csv - 1
                                            
                                    bomb_swing[columna_chart].insert(0, ubicacion_swing[2])
                                    
                                    bomb_swing[columna_chart].insert(0, ubicacion_swing[1])
                                    
                                    bomb_swing[columna_chart].insert(0, ubicacion_swing[0])
                                    
                                else:
                                    
                                    bomb_swing[columna_chart].insert(0, ubicacion_nota[2])
                                    
                                    bomb_swing[columna_chart].insert(0, ubicacion_nota[1])
                                    
                                    bomb_swing[columna_chart].insert(0, ubicacion_nota[0])
                                    
                            #cola bomb_swing x3
                            
                            elif chart[columna_nota] == "1":
                                
                                if not bomb_swing[columna_chart]:
                                    
                                    if ("1" in chart_notas or "M" in chart_notas or "2" in chart_notas or "4" in chart_notas):
                                        
                                        ubicacion_swing = ubicacion_nota.copy()
                                        
                                        for x in range(math.ceil((swingx3_inicio/4)*tipo_csv)):
                                            
                                            ubicacion_swing[2] -= aumento_disminucion_nota
                                            
                                            if ubicacion_swing[2] == -aumento_disminucion_nota:
                                                
                                                ubicacion_swing[1] -= 1
                                                
                                                ubicacion_swing[2] = resolucion - aumento_disminucion_nota
                                                
                                            if ubicacion_swing[1] == -1:
                                                
                                                ubicacion_swing[0] -= 1
                                                
                                                ubicacion_swing[1] = tipo_csv - 1
                                                
                                        bomb_swing[columna_chart].insert(0, ubicacion_swing[2])
                                        
                                        bomb_swing[columna_chart].insert(0, ubicacion_swing[1])
                                        
                                        bomb_swing[columna_chart].insert(0, ubicacion_swing[0])
                                
                                if bomb_swing[columna_chart]:
                                    
                                    if ("1" in chart_notas or "M" in chart_notas):
                                        
                                        ubicacion_swing = ubicacion_nota.copy()
                                        
                                        for x in range(math.ceil((swingx3_final/4)*tipo_csv)):
                                            
                                            ubicacion_swing[2] += aumento_disminucion_nota
                                            
                                            if ubicacion_swing[2] == resolucion:
                                                
                                                ubicacion_swing[1] += 1
                                                
                                                ubicacion_swing[2] = 0
                                                
                                            if ubicacion_swing[1] == tipo_csv:
                                                
                                                ubicacion_swing[0] += 1
                                                
                                                ubicacion_swing[1] = 0
                                                
                                        temporal_final_bomb_swing[columna_chart].append(ubicacion_swing[0])
                                        
                                        temporal_final_bomb_swing[columna_chart].append(ubicacion_swing[1])
                                        
                                        temporal_final_bomb_swing[columna_chart].append(ubicacion_swing[2])
                                        
                                        cantidad_notas += 1
                                        
                                        bomb_swing[columna_chart].insert(0, 1)
                                        
                                        #aleatorio = random.choice(valores_random)
                                        
                                        bomb_swing[columna_chart].insert(0, "12,"+str(42)+","+str(valor_raro)+","+str(242)+","+str(valor_raro-10))
                                        
                                        valor_raro -= 25
                                        
                                    elif ("2" in chart_notas or "4" in chart_notas):
                                        
                                        temporal_final_bomb_swing[columna_chart].append(1)
                                        
                                    else:
                                        
                                        if "3" in chart_notas:
                                            
                                            ubicacion_swing = ubicacion_nota.copy()
                                            
                                            for x in range(math.ceil((swingx3_final_cola/4)*tipo_csv)):
                                                
                                                ubicacion_swing[2] += aumento_disminucion_nota
                                                
                                                if ubicacion_swing[2] == resolucion:
                                                    
                                                    ubicacion_swing[1] += 1
                                                    
                                                    ubicacion_swing[2] = 0
                                                    
                                                if ubicacion_swing[1] == tipo_csv:
                                                    
                                                    ubicacion_swing[0] += 1
                                                    
                                                    ubicacion_swing[1] = 0
                                                    
                                            bomb_swing[columna_chart].insert(3, ubicacion_swing[2])
                                            
                                            bomb_swing[columna_chart].insert(3, ubicacion_swing[1])
                                            
                                            bomb_swing[columna_chart].insert(3, ubicacion_swing[0])
                                            
                                        else:
                                            
                                            bomb_swing[columna_chart].insert(3, ubicacion_nota[2])
                                            
                                            bomb_swing[columna_chart].insert(3, ubicacion_nota[1])
                                            
                                            bomb_swing[columna_chart].insert(3, ubicacion_nota[0])
                                            
                                        cantidad_notas += 1
                                        
                                        bomb_swing[columna_chart].insert(0, 1)
                                        
                                        #aleatorio = random.choice(valores_random)
                                        
                                        bomb_swing[columna_chart].insert(0, "12,"+str(42)+","+str(valor_raro)+","+str(242)+","+str(valor_raro-10))
                                        
                                        valor_raro -= 25
                                        
                                        for x in range(len(bomb_swing[columna_chart])):
                                            
                                            swing_x3.insert(0, bomb_swing[columna_chart].pop())
                                            
                                            
                                        temp_swingx3_notas[columna_chart][0] = 0
                                        

                                        
                            #swing x3 normal (cabeza)
                            
                            elif chart[columna_nota] == "2":
                                
                                if bomb_swing[columna_chart]:
                                    
                                    bomb_swing[columna_chart].clear()
                                
                                cantidad_notas += 1
                                
                                swing_x3.insert(0, ubicacion_nota[2])
                                
                                swing_x3.insert(0, ubicacion_nota[1])
                                
                                swing_x3.insert(0, ubicacion_nota[0])
                                
                            #swing x3 normal (cola)
                            
                            elif chart[columna_nota] == "3":
                                
                                swing_x3.insert(3, ubicacion_nota[2])
                                
                                swing_x3.insert(3, ubicacion_nota[1])
                                
                                swing_x3.insert(3, ubicacion_nota[0])
                                
                                swing_x3.insert(0, 1)
                                
                                #aleatorio = random.choice(valores_random)
                                
                                swing_x3.insert(0, "12,"+str(42)+","+str(valor_raro)+","+str(242)+","+str(valor_raro-10))
                                
                                valor_raro -= 25
                                
                                temp_swingx3_notas[columna_chart][0] = 0
                                
                            
                            comprobar_si_swingx3 = 0
                            
                            for a in range(columna_chart):
                                
                                comprobar_si_swingx3 = comprobar_si_swingx3 or temp_swingx3_notas[a][0]
                                
                            if not comprobar_si_swingx3:
                                
                                if temp_swingx3_notas[columna_chart][2]:
                                    
                                    cantidad_valores = len(temp_swingx3_notas[columna_chart][2])
                                    
                                    for b in range(8):
                                        
                                        b = abs(b-7)
                                        
                                        notas.insert(0, swing_x3.pop(b))
                                
                                    for elementos in range(cantidad_valores):
                                        
                                        notas.insert(0, temp_swingx3_notas[columna_chart][2].pop())
                                    
                                    
                            
                        #codigo swing_x2
                        
                        case 1:
                            
                            if chart[columna_nota] in ["1","2","3"]:
                                
                                swing_x2[columna_chart] = [1, chart[columna_nota]]
                                
                            elif chart[columna_nota] == "0" and (swing_x2[columna_chart][1] in ["1","3"]):
                                
                                swing_x2[columna_chart] = [0,0]
                                
                        #codigo nota_rojo
                        
                        case 2:
                            
                            if chart[columna_nota] in ["1","2","3"]:
                                
                                nota_rojo[columna_chart] = [1, chart[columna_nota]]
                                
                            elif chart[columna_nota] == "0" and (nota_rojo[columna_chart][1] in ["1","3"]):
                                
                                nota_rojo[columna_chart] = [0,0]
                            
                        case _:
                            """
                            if fila_nota[7] in ["M", "2"] and not swingx3_activador_conteo:
                                
                                swingx3_activador_conteo = 1
                                
                                print("hay swing antes de codigo nota")
                                
                            if fila_nota[7] == "1" and not swingx3_activador_conteo:
                                
                                swingx3_activador_2_conteo = 1
                            """
                            
                            nueva_columna_nota = columna_nota - 3
                            
                            if chart[columna_nota] == "1" or chart[columna_nota] == "M":  #si la nota es "1"
                                
                                cantidad_notas += 1
                                """
                                if swingx3_activador_conteo or swingx3_activador_2_conteo:
                                    
                                    print(ubicacion_nota, columna_nota)
                                    
                                    total_swingx3 += 1
                                """
                                for x in range(2): #repetimos 2 veces para guardar la ubicacion de una nota sin cola en csv
                                    
                                    nota.insert(0, ubicacion_nota[2])  #tercera ubicación segun resolucion
                                    
                                    nota.insert(0, ubicacion_nota[1])  #segunda ubicación en MADI
                                    
                                    nota.insert(0, ubicacion_nota[0]) #primera ubicación (para saber en que MADI estamos)
                                    
                                aleatorio = valores_random[nueva_columna_nota]   #escoger un valor random para colocar en csv
                                
                                
                                nota.insert(0, abs(nueva_columna_nota -3))    #añadimos la nota según ubicación (z = 3, x = 2 , c = 1 , v = 0)
                                                            
                                if chart[columna_nota] == "1": #para saber si es nota normal o nota mina
                                    
                                    tipo_nota = 0 + nota_solo[columna_chart][0] + (swing_x2[columna_chart][0] * 4) + (nota_rojo[columna_chart][0] * 6)
                                    
                                else: #nota mina
                                    
                                    tipo_nota = 6 + nota_solo[columna_chart][0] + (swing_x2[columna_chart][0] * 4)
                                    
                                nota.insert(0, str(tipo_nota)+","+str(aleatorio)+","+str(valor_raro)+","+str(aleatorio+50)+","+str(valor_raro-10))
                                                        
                                #añadimos valores (tipo de nota, valor random, valor raro, valor random+50, valor raro-10)
                                
                                for a in range(columna_chart):
                                    
                                    if temp_swingx3_notas[a][0] == 1:
                                        
                                        for elementos in range(len(nota)):
                                            
                                            temp_swingx3_notas[columna_chart][2].insert(0, nota.pop())
                                            
                                        break
                                
                                try:
                                    
                                    for elementos in range(len(nota)):
                                        
                                        notas.insert(0, nota.pop())
                                        
                                except:
                                    
                                    pass
                                
                                valor_raro -= 25 #valor raro lo disminuimos -25
                                
                            
                            elif chart[columna_nota] == "2" or chart[columna_nota] == "4":
                                
                                cantidad_notas += 1
                                """
                                if swingx3_activador_conteo or swingx3_activador_2_conteo:
                                    
                                    total_swingx3 += 1
                                    
                                    print(ubicacion_nota, columna_nota)
                                """
                                
                                notas_cola[columna_chart][nueva_columna_nota].extend([abs(nueva_columna_nota -3),ubicacion_nota[0],ubicacion_nota[1],ubicacion_nota[2]])
                                
                                if chart[columna_nota] == "2":
                                    
                                    notas_cola[columna_chart][nueva_columna_nota].append(2 + nota_solo[columna_chart][0] + (swing_x2[columna_chart][0] * 2) + (nota_rojo[columna_chart][0] * 6))
                                    
                                else:
                                    
                                    notas_cola[columna_chart][nueva_columna_nota].append(8 + nota_solo[columna_chart][0] + (swing_x2[columna_chart][0] * 2))
                                    

                            elif chart[columna_nota] == "3":
                                
                                
                                
                                aleatorio = valores_random[nueva_columna_nota]
                                
                                
                                tipo_nota = notas_cola[columna_chart][nueva_columna_nota].pop()
                                
                                notas_cola[columna_chart][nueva_columna_nota].extend([ubicacion_nota[0],ubicacion_nota[1],ubicacion_nota[2]])
                                
                                notas_cola[columna_chart][nueva_columna_nota].insert(0, str(tipo_nota)+","+str(aleatorio)+","+str(valor_raro)+","+str(aleatorio+50)+","+str(valor_raro-10))
                                
                                valor_raro -= 25
                                
                                for elementos in range(8):
                                    
                                    notas.insert(0, notas_cola[columna_chart][nueva_columna_nota].pop())
                                    
            """
            if fila_nota[5] in ["1", "3"] and swingx3_activador_conteo:
                    
                swingx3_activador_conteo = 0
                
                print("desativar swing activador")
                
            swingx3_activador_2_conteo = 0
            """
            #print(fila_nota,end=" ")
            
            #print(ubicacion_nota)
            
            #luego de terminar vamos aumentando los valores de la ubicaicón de la fila
            
            ubicacion_nota[2] += aumento_nota #aumento en la tercera ubicación
            
            if ubicacion_nota[2] == resolucion: #si la tercera ubicación era igual a resolución, aumenta segunda ubicación y tercera ubicación = 0
                
                ubicacion_nota[1] += 1
            
                ubicacion_nota[2] = 0
                
            if ubicacion_nota[1] == tipo_csv: #si la segunda ubicación es igual al tipo_csv se aumenta primera ubicacion y segunda ubicacion = 0
                
                ubicacion_nota[0] += 1
                
                ubicacion_nota[1] = 0
        
        #ubicacion_nota[0] += 1  #cuando termina cada sección MADI, se aumenta 
        
        #ubicacion_nota[1] = 0 #la segunda nota se setea a 0 para nuevo calculo
    
    #parte del codigo de cola bomb_swing x3
    """
    if temporal_final_bomb_swing:
        
        bomb_swing.append(temporal_final_bomb_swing[0])
        
        bomb_swing.append(temporal_final_bomb_swing[1])
        
        bomb_swing.append(temporal_final_bomb_swing[2])
        
        temporal_final_bomb_swing.clear()
        
        for x in range(len(bomb_swing)):
            
            swing_x3.insert(0, bomb_swing.pop())
    """
    for columna_temporal_final_bomb_swing in range(len(temporal_final_bomb_swing)):
        
        if temporal_final_bomb_swing[columna_temporal_final_bomb_swing]:
            
            bomb_swing[columna_temporal_final_bomb_swing].append(temporal_final_bomb_swing[columna_temporal_final_bomb_swing][0])
            
            bomb_swing[columna_temporal_final_bomb_swing].append(temporal_final_bomb_swing[columna_temporal_final_bomb_swing][1])
            
            bomb_swing[columna_temporal_final_bomb_swing].append(temporal_final_bomb_swing[columna_temporal_final_bomb_swing][2])
            
            temporal_final_bomb_swing[columna_temporal_final_bomb_swing].clear()
            
            for x in range(len(bomb_swing[columna_temporal_final_bomb_swing])):
                
                swing_x3.insert(0, bomb_swing[columna_temporal_final_bomb_swing].pop())
                
                
                
            temp_swingx3_notas[columna_temporal_final_bomb_swing][0] = 0
            
            comprobar_si_swingx3 = 0
            
            for a in range(columna_temporal_final_bomb_swing):
                
                comprobar_si_swingx3 = comprobar_si_swingx3 or temp_swingx3_notas[a][0]
                
            if not comprobar_si_swingx3:
                
                if temp_swingx3_notas[columna_temporal_final_bomb_swing][2]:
                    
                    cantidad_valores = len(temp_swingx3_notas[columna_temporal_final_bomb_swing][2])
                    
                    for b in range(8):
                        
                        b = abs(b-7)
                        
                        notas.insert(0, swing_x3.pop(b))
                
                    for elementos in range(cantidad_valores):
                        
                        notas.insert(0, temp_swingx3_notas[columna_temporal_final_bomb_swing][2].pop())
                    
                
    
    #metemos todos los swing_x3 dentro de las notas
    
    for elementos in range(len(swing_x3)):
        
        notas.insert(0, swing_x3.pop())
    
    #print(notas)
    
    print(total_swingx3)
    
    return notas,cantidad_notas,resolucion

def guardar_datos_csv(notas_convertidas,bpm,cantidad_madi,cantidad_notas, resolucion):
    
    valores_csv = ["300",
                   "768",
                   "2,1",
                   "SM2CSV ver, RT",
                   "Chart by, "+nombre_creador,
                   str(bpm)+","+str(cantidad_madi)+","+str(tipo_csv)+","+str(resolucion),
                   "0.48,60",str(cantidad_notas)]
    
    with open(nombre_archivo+".csv","w") as archivo:
        
        for valor in valores_csv:
            
            archivo.write(valor+"\n")
            
        for item in notas_convertidas:
        
            valor_agregar = str(item)+"\n"
        
            archivo.write(valor_agregar)
            
        archivo.write("0")
        
def guardar_datos_sm(notas):
    
    with open("datos_single_bpm.sm","w") as archivo:
        
        for madi in notas:
            
            for fila_madi in madi:
                
                valor_agregar = str(fila_madi)+"\n"
                
                archivo.write(valor_agregar)
                
            archivo.write(",\n")
    
def main():
    
    datos_sm = abrir_sm()
    
    notas,bpm,cantidad_madi, swingx3_inicio, swingx3_final, swingx3_final_cola = extraer_notas(datos_sm)
    
    pregunta = input("¿Quieres guardar los datos en [1]SM o [2]CSV?: ")
    
    if pregunta == "1":
        
        guardar_datos_sm(notas)
        
    else:
    
        notas_convertidas,cantidad_notas,resolucion = convertir_notas(notas, swingx3_inicio, swingx3_final, swingx3_final_cola)
        
        guardar_datos_csv(notas_convertidas,bpm,cantidad_madi,cantidad_notas, resolucion)
    
if __name__ == "__main__":
    
    main()